import React, {useState, useEffect} from 'react'
import { Layout, Menu, Breadcrumb } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import "antd/dist/antd.css";


const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

const handleNav1Click = (e) => {
    console.log(e)
}

class HomePage extends React.Component{
    constructor(props){
        super(props)
        this.props = props
        this.callback = props.func
        this.changeNav = this.changeNav.bind(this)
        this.state = {content: 'Content', content2: 'nest', func: ()=> console.log('asdads')}
        this.prosireniState = {...this.state, nesto: 'drugo'}
        console.log(Object.keys(this.state))
        console.log(this.state)
    }

    componentDidMount(){
        console.log('Component Mounted')
        // result = axios.get('')
        // nakon fatch this.setState({content: result.data})
    }

    componentDidUpdate(){
        // result = axios.set('',this.data)
        console.log('Comonent updated')
    }

    changeNav(e){
        console.log(this.content)
        this.setState({...this.state, content: 'Nav '+e.key})
        // this.content = 'Nav '+e.key
    }


    render() {
        return <div>
        <Layout>
            <Header className="header">
            <div className="logo" />
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                <Menu.Item key="1" onClick={this.changeNav}>nav 1</Menu.Item>
                <Menu.Item key="2" onClick={this.changeNav}>nav 2</Menu.Item>
                <Menu.Item key="3" onClick={this.changeNav}>nav 3</Menu.Item>
            </Menu>
            </Header>
            <Layout>
            <Sider width={200} className="site-layout-background">
                <Menu
                mode="inline"
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub1']}
                style={{ height: '100%', borderRight: 0 }}
                >
                <SubMenu key="sub1" icon={<UserOutlined />} title="subnav 1">
                    <Menu.Item key="1">option1</Menu.Item>
                    <Menu.Item key="2">option2</Menu.Item>
                    <Menu.Item key="3">option3</Menu.Item>
                    <Menu.Item key="4">option4</Menu.Item>
                </SubMenu>
                <SubMenu key="sub2" icon={<LaptopOutlined />} title="subnav 2">
                    <Menu.Item key="5">option5</Menu.Item>
                    <Menu.Item key="6">option6</Menu.Item>
                    <Menu.Item key="7">option7</Menu.Item>
                    <Menu.Item key="8">option8</Menu.Item>
                </SubMenu>
                <SubMenu key="sub3" icon={<NotificationOutlined />} title="subnav 3">
                    <Menu.Item key="9">option9</Menu.Item>
                    <Menu.Item key="10">option10</Menu.Item>
                    <Menu.Item key="11">option11</Menu.Item>
                    <Menu.Item key="12">option12</Menu.Item>
                </SubMenu>
                </Menu>
            </Sider>
            <Layout style={{ padding: '0 24px 24px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>List</Breadcrumb.Item>
                <Breadcrumb.Item>App</Breadcrumb.Item>
                </Breadcrumb>
                <Content
                className="site-layout-background"
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280,
                }}
                >
                {this.state.content}
                </Content>
            </Layout>
            </Layout>
        </Layout>
    </div>
    }
}

function getFunc(){

} 

const HomePageTwo = (props) => {
    console.log('lambda props', props)
    const [content, setContent] = useState('Content')
    useEffect(getFunc,[content])

    return (
    <div>
        <Layout>
            <Header className="header">
            <div className="logo" />
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                <Menu.Item key="1" onClick={(e)=> setContent('Nav '+e.key)}>nav 1</Menu.Item>
                <Menu.Item key="2" onClick={(e)=> setContent('Nav '+e.key)}>nav 2</Menu.Item>
                <Menu.Item key="3" onClick={(e)=> setContent('Nav '+e.key)}>nav 3</Menu.Item>
            </Menu>
            </Header>
            <Layout>
            <Sider width={200} className="site-layout-background">
                <Menu
                mode="inline"
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub1']}
                style={{ height: '100%', borderRight: 0 }}
                >
                <SubMenu key="sub1" icon={<UserOutlined />} title="subnav 1">
                    <Menu.Item key="1">option1</Menu.Item>
                    <Menu.Item key="2">option2</Menu.Item>
                    <Menu.Item key="3">option3</Menu.Item>
                    <Menu.Item key="4">option4</Menu.Item>
                </SubMenu>
                <SubMenu key="sub2" icon={<LaptopOutlined />} title="subnav 2">
                    <Menu.Item key="5">option5</Menu.Item>
                    <Menu.Item key="6">option6</Menu.Item>
                    <Menu.Item key="7">option7</Menu.Item>
                    <Menu.Item key="8">option8</Menu.Item>
                </SubMenu>
                <SubMenu key="sub3" icon={<NotificationOutlined />} title="subnav 3">
                    <Menu.Item key="9">option9</Menu.Item>
                    <Menu.Item key="10">option10</Menu.Item>
                    <Menu.Item key="11">option11</Menu.Item>
                    <Menu.Item key="12">option12</Menu.Item>
                </SubMenu>
                </Menu>
            </Sider>
            <Layout style={{ padding: '0 24px 24px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>List</Breadcrumb.Item>
                <Breadcrumb.Item>App</Breadcrumb.Item>
                </Breadcrumb>
                <Content
                className="site-layout-background"
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280,
                }}
                >
                {content}
                </Content>
            </Layout>
            </Layout>
        </Layout>
    </div>

    )
}

export {HomePageTwo}
export default HomePage