import './App.css';

import Home, {HomePageTwo} from './views/Home/Home'

function App() {
  const value = 2
  return (
    <div>
      <Home nesto='string1' val={value} func={nekaRandomComp}></Home>
      <HomePageTwo nesto='string2' val={value} func={nekaRandomComp}></HomePageTwo>
    </div>
  );
}

function nekaRandomComp(param = 0) {
  console.log('NEKA RAND FUNC 300 ')
  return param+1
}

export default App;
